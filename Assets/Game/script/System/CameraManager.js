﻿#pragma strict
private var Cameras : GameObject[];
static var ActiveCamera : Camera;

function Awake () 
{
	var tempCam = new Array();
	for (var go in FindObjectsOfType(GameObject))
	{
		if (go.GetComponent(Camera) != null)
		{
			tempCam.Add(go);
		}
	}
	Cameras = tempCam;
	for (var cam in Cameras)
	{
		cam.camera.enabled = false;
	}
}

function Start () 
{

}

function Update () 
{

}

function ActivateCam(CurrentGamePhase : GamePhase)
{
	for (var cam in Cameras)
	{
		cam.camera.enabled = false;
	}
	
	switch (CurrentGamePhase)
	{
		case GamePhase.Briefing : 
			ActiveCamera = GameObject.Find("ScreenBriefing/Cam").camera;
			break;
		case GamePhase.Planification : 
			ActiveCamera = GameObject.Find("ScreenPlanification/Cam").camera;
			break;
		case GamePhase.LutteInfluence : 
			if (NetworkMasterBH.PlayerSide == PlayerSides.Blue)
				ActiveCamera = GameObject.Find("ScreenInfluence/BlueCam").camera;
			else
				ActiveCamera = GameObject.Find("ScreenInfluence/RedCam").camera;
			break;
		case GamePhase.ResolutionConflit : 
			ActiveCamera = GameObject.Find("ScreenResolution/Cam").camera;
			break;
	}
	ActiveCamera.enabled = true;
}

@ RPC function swapCam() // uniquement pendant la lutte d'influence
{
	ActiveCamera.enabled = false;
	if (ActiveCamera == GameObject.Find("ScreenInfluence/BlueCam").camera)
	{
		ActiveCamera = GameObject.Find("ScreenInfluence/RedCam").camera;
	}
	else
	{
		ActiveCamera = GameObject.Find("ScreenInfluence/BlueCam").camera;
	}
	ActiveCamera.enabled = true;
}