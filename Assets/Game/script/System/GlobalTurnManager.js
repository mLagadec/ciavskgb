﻿public var VictoryScores : int[];

enum GamePhase 
{
	Briefing,
	Planification,
	LutteInfluence,
	ResolutionConflit,
	Debriefing,
	Detente,
	
}
public var CurrentGamePhase : GamePhase;

enum ActivePlayer
{
	Red = 0,
	Blue = 1,
	Undertermined = 2,
}

public var CurrentFirstPlayer : ActivePlayer;
public var CurrentActivePlayer : ActivePlayer;
public var CurrentDominationMarker : ActivePlayer;

public var CurrentObjectif : GameObject;

public var BlueValidation : boolean;
public var RedValidation : boolean;

function Awake ()
{
	VictoryScores = new int[2];
	VictoryScores[ActivePlayer.Red] = 0;
	VictoryScores[ActivePlayer.Blue] = 0;
}

function Start () 
{
	
}

function Update () 
{

}

function GetPlayerValidation (Side : PlayerSides)
{
	if (Side == PlayerSides.Blue)
		BlueValidation = true;
	else // Side = Red
		RedValidation = true;
		
	if (BlueValidation == true && RedValidation == true && Network.isServer == true)
		NextPhase ();
}	

function NextPhase()
{
	switch (CurrentGamePhase.ToString())
	{
		case "Briefing":
			break;
		case "Planification":
			break;
		case "LutteInfluence":
			break;
		case "ResolutionConflit":
			break;
		case "Debriefing":
			break;
		case "Detente":
			break;
	}
}

function LaunchBriefing ()
{
	//tirer une carte objectif
	networkView.RPC("TakeFirstObjectif",RPCMode.All);
	
	//déterminer premier joueur
	if (CurrentDominationMarker == ActivePlayer.Undertermined) // s'il y a eu désordre civil pour les deux joueurs au tour précédent
	{
		// CurrentFirstPlayer = CurrentFirstPlayer;
	}
	else if (VictoryScores[ActivePlayer.Red] > VictoryScores[ActivePlayer.Blue]) // Rouge gagnant >> Bleu en premier
	{
		CurrentFirstPlayer = ActivePlayer.Blue;
	}
	else if (VictoryScores[ActivePlayer.Red] < VictoryScores[ActivePlayer.Blue]) // Bleu gagnant >> Rouge en premier
	{
		CurrentFirstPlayer = ActivePlayer.Red;
	}
	else if (VictoryScores[ActivePlayer.Red] == 0 && VictoryScores[ActivePlayer.Blue] == 0) // premier tour
	{
		var randomPlayer : int;
		randomPlayer = Mathf.RoundToInt(Random.value);
		CurrentFirstPlayer = randomPlayer;
	}
	else if (VictoryScores[ActivePlayer.Red] == VictoryScores[ActivePlayer.Blue]) // si égalité 
	{
		CurrentFirstPlayer = CurrentDominationMarker;
	}
	
	GameObject.Find("GroupDeck").SendMessage("shuffleArray");
	
}

@ RPC function TakeFirstObjectif()
{
	CurrentObjectif = ObjectifDeck.CurrentObjectifDeck[0];
	var tempDeck = new Array();
	tempDeck = ObjectifDeck.CurrentObjectifDeck;
	tempDeck.Shift();
	ObjectifDeck.CurrentObjectifDeck = tempDeck;
}

function GroupDeckShuffled ()
{
	networkView.RPC("StartBriefing",RPCMode.All, CurrentFirstPlayer.ToString()); 
}

@ RPC function StartBriefing(first : String)
{
	if (!Network.isServer)
	{
		if (first == "Red")	
			CurrentFirstPlayer = ActivePlayer.Red;
		else
			CurrentFirstPlayer = ActivePlayer.Blue;
	}
	
	CurrentGamePhase = GamePhase.Briefing;
	gameObject.SendMessage("ActivateCam",CurrentGamePhase);

}