﻿#pragma strict
enum Sides
{
	Blue,
	Red,
}
public var Side : Sides;

enum Roles 
{
	Maitre_Espion = 1,
	Directeur_Adjoint = 2,
	Agent_Double = 3,
	Analyste = 4,
	Assassin = 5,
	Directeur = 6,
}
public var Role : Roles;
@Range(1,6) public var Initiative : int;

function Awake ()
{
	if (transform.parent.gameObject.name == "AgentsRed")
	{
		Side = Sides.Red;
	}
	else 
	{
		Side = Side.Blue;
	}
	gameObject.name = "0"+Initiative.ToString()+" "+Role.ToString();
}

function Start () 
{

}

function Update () {

}