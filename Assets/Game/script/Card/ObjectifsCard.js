﻿#pragma strict

public var Name : String;

enum Sectors 
{
	Economic,
	Mediatic,
	Military,
	Politic,
}
public var Profile : Sectors[];

enum StabilityThresholds
{
	_6 = 6,
	_9 = 9,
	_11 = 11,
	_13 = 13,
}
public var Stabilite : StabilityThresholds;

enum PopulationThresholds
{
	_1 = 1,
	_3 = 3,
	_4 = 4,
	_5 = 5,
}
public var Population : PopulationThresholds;

enum VictoryPointsThresholds
{
	_5 = 5,
	_10 = 10,
	_15 = 15,
	_20 = 20,
}
public var VictoryPoints : VictoryPointsThresholds;

enum Controllers
{
	inDeck,
	InPlay,
	RedPlayer,
	BluePlayer,
}
public var ControlledBy : Controllers;

function Awake ()
{
	gameObject.name = Name;
}


function Start () 
{

}

function Update () 
{

}