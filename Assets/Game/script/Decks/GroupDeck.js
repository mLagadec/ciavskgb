﻿#pragma strict

public var InspectorDeck : GameObject [];
static var CurrentGroupDeck : GameObject [];

function Awake () 
{

}

function Start () 
{

}

function Update () 
{

}

function shuffleArray() 
{
    networkView.RPC("reInitDeck",RPCMode.All);
	
	for (var i : int = CurrentGroupDeck.length - 1; i > 0; i--) 
	{
        var j = Mathf.FloorToInt(Random.value * (i + 1));
        networkView.RPC("sweepObject",RPCMode.All, i, j);
    }
	
	GameObject.Find("_SystemManager").SendMessage("GroupDeckShuffled");
}

@ RPC function reInitDeck()
{
	CurrentGroupDeck = new Array (InspectorDeck);
}

@ RPC function sweepObject(indexTested : int, newRandomIndex : int)
{
	var temp = CurrentGroupDeck[indexTested];	
	CurrentGroupDeck[indexTested] = CurrentGroupDeck[newRandomIndex];
	CurrentGroupDeck[newRandomIndex] = temp;
}