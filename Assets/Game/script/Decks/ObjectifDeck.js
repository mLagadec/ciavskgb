﻿#pragma strict

public var InspectorDeck : GameObject [];
static var CurrentObjectifDeck : GameObject [];

function Awake () 
{

}

function Start () 
{
	
}

function Update () 
{

}

function shuffleArray() 
{
    networkView.RPC("reInitDeck",RPCMode.All);
	
	for (var i : int = CurrentObjectifDeck.length - 1; i > 0; i--) 
	{
        var j = Mathf.FloorToInt(Random.value * (i + 1));
        networkView.RPC("sweepObject",RPCMode.All, i, j);
    }
	
	GameObject.Find("_SystemManager").SendMessage("ObjectifDeckShuffled");
}

@ RPC function reInitDeck()
{
	CurrentObjectifDeck = new Array (InspectorDeck);
}

@ RPC function sweepObject(indexTested : int, newRandomIndex : int)
{
	var temp = CurrentObjectifDeck[indexTested];	
	CurrentObjectifDeck[indexTested] = CurrentObjectifDeck[newRandomIndex];
	CurrentObjectifDeck[newRandomIndex] = temp;
}