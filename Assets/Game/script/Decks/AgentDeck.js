﻿#pragma strict

public var InspectorDeck : GameObject [];
static var CurrentAgentDeck : GameObject [];

static var CurrentAgent : GameObject;
public var AgentAtRest : GameObject[];

public var AgentsDead : GameObject[];

function Awake () 
{
	CurrentAgentDeck = new Array (InspectorDeck);
}

function Start () 
{

}

function Update () 
{

}

function PutCurrentAgentAtRest ()
{
	
	//stock l'agent courant dans les agents au repos
	var tempDeck01 = new Array();
	AgentAtRest = tempDeck01;
	AgentAtRest[0] = CurrentAgent;
	
	// remove agent at rest from currentdeck
	var tempDeck02 = new Array();
	tempDeck02 = CurrentAgentDeck;
	var i = 0;
	for (var agent in tempDeck02)
	{
		if (agent == AgentAtRest[0])
		{
			tempDeck02.RemoveAt(i);
		}
		i ++;
	}
	
	tempDeck02.Sort();
	CurrentAgentDeck = tempDeck02;
	
}

function PutAgentFromDeathToRest (TargetIndex : int)
{
	// get a dead agent and put it in Rest
	var tempDeck03 = new Array();
	tempDeck03 = AgentAtRest;
	tempDeck03.Add(AgentsDead[TargetIndex]);
	tempDeck03.Sort();
	AgentAtRest = tempDeck03;

}

function PutAgentFromRestToAction ()
{
	//remet les agents au repos dans le deck
	if (AgentAtRest.length != null)
	{
		var tempDeck = new Array();
		tempDeck = CurrentAgentDeck;
		var j = 0;
		for (var agent in AgentAtRest)
		{
			tempDeck.Add(AgentAtRest[j]);
			j ++;
		}
		CurrentAgentDeck = tempDeck;
	}
}