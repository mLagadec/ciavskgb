﻿public var LevelLoader : GameObject;

public var server : boolean;
public var listenPort : int; //le port d'écoute du serveur
public var remoteIP : String; //l'adresse IP du serveur auquel les clients se connecteront

public var cuby : GameObject;
private var cubyInst : GameObject;

function Awake()
{
	DontDestroyOnLoad(this);
}


function TestServer () 
{
	if(server)
	{
		Network.InitializeServer(32, listenPort, false); //le false signifie qu'on utilise pas le Nat punchtrough. Je vous recommande la doc d'Unity pour en savoir plus
		  
		// On préviens tous nos objets que le réseau est lancé
		for (var go in FindObjectsOfType(GameObject))
		{
			go.SendMessage("OnNetworkLoadedLevel", SendMessageOptions.DontRequireReceiver);
		}
	}
	else
	{
		Network.Connect(remoteIP, listenPort);	
	}
	//Application.LoadLevelAdditive("TestRéseaux_Level");
}

function Update () 
{

}

function OnPlayerConnected(Player: NetworkPlayer)
{
	if(server)
	{
		//print("new player ! from : "+Player.ipAddress);
			
		Instantiate (LevelLoader,transform.position,transform.rotation);
	}
}

function OnConnectedToServer()
{
	Instantiate (LevelLoader,transform.position,transform.rotation);
	print ("connecté au serveur");
}


function OnNewLevelLoaded()
{
	if (Application.isLoadingLevel == true)
	{
		recallOnlevelLoaded ();
		return;
	}
	
	if ( Application.loadedLevelName == "TestRéseaux_Menu")
	{
		Destroy(this.gameObject);
	}
	else
	{
		// Notify our objects that the level and the network are ready
		for (var go in FindObjectsOfType(GameObject))
		{
			go.SendMessage("OnNetworkLoadedLevel", SendMessageOptions.DontRequireReceiver);
		}
		var spawners = GameObject.FindGameObjectsWithTag("spawn_test");
		var rand : int = Random.Range(0, spawners.length);
		print ("spawners : "+spawners.length);
		print ("rand : "+rand);
		var spawn : GameObject = spawners[rand];
		
		cubyInst = Network.Instantiate(cuby, spawn.transform.position, Quaternion.identity, 0);
		
	}
}

function recallOnlevelLoaded ()
{
	Invoke("OnNewLevelLoaded", 0.1);
}