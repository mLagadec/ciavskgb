﻿#pragma strict

public var vitesse : float = 0.05;
public var vitesseRotation : float = 2;
public var Jump : float = 350;

public var PlayerCamera : GameObject;

public var CanJump : boolean;

function Awake () 
{
	if(!networkView.isMine)
	{
		this.enabled = false;
		Destroy(PlayerCamera);
	}
	CanJump = true;
}

function Update ()
{
	Deplacement();
}

function Deplacement()
{
	if ( Input.GetKey(KeyCode.Z) || Input.GetKey(KeyCode.UpArrow) )
		transform.Translate(Vector3.forward * vitesse);
	else if ( Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow) )
		transform.Translate(Vector3.back * vitesse);
	
	if ( Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.LeftArrow) )
		transform.Rotate(Vector3.up * vitesseRotation);
	else if ( Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow) )
		transform.Rotate(-Vector3.up * vitesseRotation);

	if ( Input.GetKeyDown(KeyCode.Space) && CanJump) //le saut
	{
		rigidbody.AddForce(Vector3.up * Jump);
		CanJump = false;
	}
}