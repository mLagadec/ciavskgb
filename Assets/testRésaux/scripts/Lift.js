﻿private var isDown : boolean;
public var isMoving : boolean;
private var LastActivation : float;

@ Range(1,10) public var liftSmoothness : int;
public var downPosition : float;
public var upPosition : float;


function Start () 
{
	isDown = true;
	isMoving = false;
}

function Update () 
{
	if ((Time.time - LastActivation) / liftSmoothness >= 1 && isMoving == true)
	{
		isMoving = false;
		if (isDown) //is down at start >> false at end
		{
			isDown = false;
		}
		else // not down at start >> down at end
		{
			isDown = true;
		}
	}
	
	if (isMoving == true)
	{
		if (isDown) // down >> go up
		{
			transform.position.y = Mathf.Lerp(downPosition,upPosition,(Time.time - LastActivation) / liftSmoothness);
		}
		else // not down >> go down
		{
			transform.position.y = Mathf.Lerp(upPosition,downPosition,(Time.time - LastActivation) / liftSmoothness);
		}
	}
}

function Activation ()
{
	if (isMoving == false)
	{
		networkView.RPC("Activated", RPCMode.All);
	}
}

@RPC
function Activated ()
{
	print ("activated");
	isMoving = true;
	LastActivation = Time.time;
}

