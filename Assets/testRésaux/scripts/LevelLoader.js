﻿#pragma strict

public var LevelToLoad : String;

function Start () 
{
	for (var go in FindObjectsOfType(GameObject))
	{
		go.SendMessage("OnNewLevelLoaded", SendMessageOptions.DontRequireReceiver);
	}
	Application.LoadLevelAdditive(LevelToLoad);
	Destroy(gameObject);
}

function Update () 
{

}