﻿#pragma strict

public var projectile : GameObject;
public var BulletSpeed : float;

function Awake () 
{
	if (!networkView.isMine)
	{	
		Destroy(this);
	}
}

function Update () 
{
	if (Input.GetKeyDown(KeyCode.Mouse0))
	{
		Shoot();
	}
}

function Shoot ()
{
	var bullet : GameObject = Network.Instantiate(projectile, transform.position, transform.rotation, 0);
	bullet.rigidbody.velocity = transform.TransformDirection (Vector3.forward * BulletSpeed);

}

