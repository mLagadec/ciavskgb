﻿#pragma strict

private var LiftScript : Lift;

function Start () 
{
	LiftScript = transform.root.GetComponent("Lift") as Lift;
}

function Update () 
{

}

function OnTriggerEnter (col : Collider)
{
	if (col.tag == "Player" && LiftScript.isMoving == false && col.networkView.isMine)
	{
		LiftScript.Activation();
	}
}