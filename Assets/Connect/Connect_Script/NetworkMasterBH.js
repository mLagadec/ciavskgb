﻿public var remoteIP : String;
public var listenPort : int;

public var GameLoader : GameObject;
public var MenuLoader : GameObject;

enum ApplicationStates
{
	onMenu,
	inGame,
}

private var ApplicationState : ApplicationStates;

enum PlayerSides
{
	Undefined,
	Blue,
	Red,
}

static var PlayerSide : PlayerSides;

function Awake()
{
	DontDestroyOnLoad(this);
}

function Start () 
{
	ApplicationState = ApplicationStates.onMenu;
}

function Update () 
{

}

function OnNewLevelLoaded()
{
	if (Application.isLoadingLevel == true)
	{
		recallOnlevelLoaded ();
		return;
	}
	
	if (ApplicationState == ApplicationStates.inGame)
	{
		Destroy(gameObject);
	}
	else if (ApplicationState == ApplicationStates.onMenu) //lancement du jeu
	{
		ApplicationState = ApplicationStates.inGame;
		//print ("Game Launched on network");

	}
}

function recallOnlevelLoaded ()
{
	Invoke("OnNewLevelLoaded", 0.1);
}

function TestServer(server:boolean,remoteIPTemp : String,listenPortTemp : int, side : String)
{
	remoteIP = remoteIPTemp;
	listenPort = listenPortTemp;
	
	if(server)
	{
		Network.InitializeServer(32, listenPort, false); //le false signifie qu'on utilise pas le Nat punchtrough. Je vous recommande la doc d'Unity pour en savoir plus
		  
		// On préviens tous nos objets que le réseau est lancé
		for (var go in FindObjectsOfType(GameObject))
		{
			go.SendMessage("OnNetworkLoadedLevel", server,SendMessageOptions.DontRequireReceiver);
		}
		switch (side)
		{
			case "blue" : PlayerSide = PlayerSides.Blue;
				break;
			case "red" : PlayerSide = PlayerSides.Red;
				break;
			case "undefined" :
				break;
		}
	}
	else
	{
		Network.Connect(remoteIP, listenPort);	
	}
}

function OnPlayerConnected(Player: NetworkPlayer)
{
	var SideForOther : PlayerSides;
	
	if (PlayerSide == PlayerSides.Blue)
		SideForOther = PlayerSides.Red;
	else
		SideForOther = PlayerSides.Blue;
		
	networkView.RPC("DefineSide",RPCMode.Others,SideForOther.ToString());
	
	Menu_01.MenuState = MenuStates.PlayerConnecting;
}

function OnConnectedToServer()
{

}

function OnDisconnectedFromServer(info : NetworkDisconnection) 
{
	if (ApplicationState == ApplicationStates.inGame)
	{
		Instantiate (MenuLoader, transform.position, transform.rotation);
		ApplicationState = ApplicationStates.onMenu;
	}
	Menu_01.MenuState = MenuStates.DisconnectedFromServer;
	print ("déconnecté");
}

function OnPlayerDisconnected(Player: NetworkPlayer) 
{
	Network.RemoveRPCs(Player);
	Network.DestroyPlayerObjects(Player);
	if (Network.isServer)	
	{
		if (ApplicationState == ApplicationStates.inGame)
		{
			Instantiate (MenuLoader, transform.position, transform.rotation);
		}
		
		Menu_01.MenuState = MenuStates.ServerCreated;
	}
	else
	{
		if (ApplicationState == ApplicationStates.inGame)
		{
			Instantiate (MenuLoader, transform.position, transform.rotation);
		}
		
		Menu_01.MenuState = MenuStates.StartMenu;
	}
}

@ RPC function DefineSide (newSide : String)
{
	if (newSide == "Blue")
		PlayerSide = PlayerSides.Blue;
	else
		PlayerSide = PlayerSides.Red;
	Menu_01.MenuState = MenuStates.ConnectingToServer;
	print ("player is server ? : "+Network.isServer+" > get this Side :"+PlayerSide);
}

function launchGameFromMenu ()
{
	networkView.RPC("launchGameFromMenuGlobal",RPCMode.All);
}

@ RPC function launchGameFromMenuGlobal ()
{
	Instantiate (GameLoader,transform.position, transform.rotation);
	print ("gameLaunched");
}