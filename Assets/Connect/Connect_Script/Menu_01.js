﻿var networkMaster : GameObject; // Prefab

private var instantiatedMaster : GameObject; //Prefab instancié
private var scriptStartNet : NetworkMasterBH;

public var serverIP : String = "127.0.0.1";
public var serverPort : int = 25000;

enum MenuStates
{
	StartMenu,
	ServerCreated,
	PlayerConnecting,
	ConnectingToServer,
	DisconnectedFromServer,
	GameLaunched,
}
static var MenuState : MenuStates = MenuStates.StartMenu;

private var menuSizeX : int = 800;
private var menuSizeY : int = 480;
private var menuPosX : float = 20;
private var menuPosY : float = Screen.height/2 - menuSizeY/2;
private var mainMenu = new Rect(menuPosX, menuPosY, menuSizeX, menuSizeY);
private var sizeButtonX : int = 250;
private var sizeButtonY : int = 30;


function Start () 
{

}

function Update () 
{

}

function OnNetworkLoadedLevel(IsServer : boolean)
{
	print ("server créé");
	if (IsServer)
		MenuState = MenuStates.ServerCreated;
	else
		MenuState = MenuStates.ConnectingToServer;
}

function OnNewLevelLoaded ()
{
	Destroy (gameObject);
}

function OnGUI ()
{
	//Le menu de base
	switch (MenuState.ToString())
	{
		case "StartMenu" : BaseMenu ();
			break;
		case "ServerCreated" : ServerCreatedMenu();
			break;
		case "PlayerConnecting" : PlayerConnectingMenu();
			break;
		case "ConnectingToServer" : ConnectingToServerMenu();
			break;
		case "DisconnectedFromServer" : DisconnectedFromServerMenu();
			break;
		case "GameLaunched" : GameLaunchedMenu();
			break;
		default : 
			break;
	}
}

function BaseMenu ()
{
	GUI.BeginGroup(mainMenu, "");
		GUI.Box(Rect(0,0,menuSizeX, menuSizeY), "");

		//La demande de champs d'ip pour rejoindre un serveur
		GUI.TextField(new Rect(sizeButtonX+30, 60, 120, 30), serverIP, 40);

		//create as Blue
		if ( GUI.Button(Rect(10, 20, sizeButtonX, sizeButtonY), "Créer en tant que CIA"))
		{
			//Création du serveur
			if (instantiatedMaster == null)
			{
				instantiatedMaster = Instantiate(networkMaster, Vector3.zero, Quaternion.identity);
				scriptStartNet = instantiatedMaster.GetComponent("NetworkMasterBH") as NetworkMasterBH;
				scriptStartNet.TestServer (true, serverIP, serverPort, "blue");
			}
		}
		//create as Red
		if ( GUI.Button(Rect(sizeButtonX+30, 20, sizeButtonX, sizeButtonY), "Créer en tant que KGB"))
		{
			//Création du serveur
			if (instantiatedMaster == null)
			{
				instantiatedMaster = Instantiate(networkMaster, Vector3.zero, Quaternion.identity);
				scriptStartNet = instantiatedMaster.GetComponent("NetworkMasterBH") as NetworkMasterBH;
				scriptStartNet.TestServer (true, serverIP, serverPort, "red");
			}
		}
		if ( GUI.Button(Rect(10, 60, sizeButtonX, sizeButtonY), "Rejoindre serveur"))
		{
			//Rejoindre serveur
			instantiatedMaster = Instantiate(networkMaster, Vector3.zero, Quaternion.identity);
			scriptStartNet = instantiatedMaster.GetComponent("NetworkMasterBH") as NetworkMasterBH;
			scriptStartNet.TestServer (false, serverIP, serverPort, "undefined");
		}
	GUI.EndGroup();
}

function ServerCreatedMenu ()
{
	GUI.BeginGroup(mainMenu, "");
		GUI.Box(Rect(0,0,menuSizeX, menuSizeY), "");
		GUI.Box(new Rect(30, 10, 220, 30), "Server waiting for players as "+NetworkMasterBH.PlayerSide);
		GUI.Box(new Rect(30, 60, 120, 30), serverIP);
		if ( GUI.Button(Rect(sizeButtonX+30, 10, sizeButtonX, sizeButtonY), "Kill server"))
		{
			Destroy (instantiatedMaster);
			MenuState = MenuStates.StartMenu;
		}

	GUI.EndGroup();
}

function PlayerConnectingMenu()
{
	GUI.BeginGroup(mainMenu, "");
		GUI.Box(Rect(0,0,menuSizeX, menuSizeY), "");

		GUI.Box(new Rect(10, 10, 200, 30), "is ok to play with : ");
		if (Network.connections.length > 0)
		{
			GUI.Box(new Rect(sizeButtonX+30, 10, 120, 30),Network.connections[0].ipAddress.ToString() );
		}
		else 
		{
			MenuState = MenuStates.ServerCreated;
		}
		//create as Blue
		if ( GUI.Button(Rect(10, 60, sizeButtonX, sizeButtonY), "Sure"))
		{
			//launch game with both players
			instantiatedMaster.SendMessage("launchGameFromMenu");
		}
		//create as Red
		if ( GUI.Button(Rect(sizeButtonX+30, 60, sizeButtonX, sizeButtonY), "Nope"))
		{
			Network.CloseConnection(Network.connections[0], false);
			MenuState = MenuStates.ServerCreated;
		}
	GUI.EndGroup();
}

function ConnectingToServerMenu ()
{
	GUI.BeginGroup(mainMenu, "");
		GUI.Box(Rect(0,0,menuSizeX, menuSizeY), "");
		//La demande de champs d'ip pour rejoindre un serveur
		GUI.Box(new Rect(30, 60, 200, 30), "Waiting for server confirmation");
	GUI.EndGroup();
}

function DisconnectedFromServerMenu ()
{
	GUI.BeginGroup(mainMenu, "");
		GUI.Box(Rect(0,0,menuSizeX, menuSizeY), "");
		GUI.Box(new Rect(30, 60, 200, 30), "Kicked from server");
		if ( GUI.Button(Rect(sizeButtonX+30, 60, sizeButtonX, sizeButtonY), "Return to main"))
		{
			MenuState = MenuStates.StartMenu;
		}
	GUI.EndGroup();
}

function GameLaunchedMenu ()
{
	GUI.BeginGroup(mainMenu, "");
	GUI.EndGroup();
}

